import './style.less';

window.onload = () => {

    window.scroll({
        top: 0,
        left: 0, 
        behavior: 'smooth'
    });

    if ( document.documentElement.scrollTop > 30) {
        var header = document.querySelector('.header');
        header.classList.add('fixed');
    } else {
        var header = document.querySelector('.header');
        header.classList.remove('fixed');
    }

    var cardwidth;

    var setCardWidth = () => {
        if (window.innerWidth < 451) {
            cardwidth = document.querySelector('.cards').getBoundingClientRect().width - 50;
        } else {
            cardwidth = document.querySelector('.cards').getBoundingClientRect().width - 90;
        }

        document.querySelectorAll('.card_cont').forEach( (e) => {
            e.style.width = cardwidth + 'px';
        });
    };

    setCardWidth();

    var carousel = [];
    var active = 0;
    var next = 0;

    document.querySelectorAll('.card_cont').forEach( (e) => {
        carousel.push(e);
        e.style.width = cardwidth + 'px';
        e.classList.add('animated');
    });

    var ponent_prev = document.getElementById('ponents_prev');
    var ponent_next = document.getElementById('ponents_next');

    ponent_prev.onclick = (e) => {
        e.preventDefault();
        carousel[active].classList.add('fadeOutRight');
        next = active - 1;
        if (next < 0) {
            next = carousel.length - 1;
        }

        setTimeout(() => {
            carousel[active].classList.remove('active');
            carousel[next].classList.add('active');
            carousel[next].classList.add('fadeInLeft');
            carousel[active].classList.remove('fadeOutRight');
            active = next;
            setTimeout(() => {
                carousel[next].classList.remove('fadeInLeft');
            }, 1000);
        }, 750);
    };
    
    ponent_next.onclick = (e) => {
        e.preventDefault();
        carousel[active].classList.add('fadeOutLeft');
        next = active + 1;
        if (next === carousel.length) {
            next = 0;
        }

        setTimeout(() => {
            carousel[active].classList.remove('active');
            carousel[next].classList.add('active');
            carousel[next].classList.add('fadeInRight');
            carousel[active].classList.remove('fadeOutLeft');
            active = next;
            setTimeout(() => {
                carousel[next].classList.remove('fadeInRight');
            }, 1000);
        }, 750);
    };

    document.onscroll = (e) => {
        if ( document.documentElement.scrollTop > 30) {
            var header = document.querySelector('.header');
            header.classList.add('fixed');
        } else {
            var header = document.querySelector('.header');
            header.classList.remove('fixed');
        }
    };

    window.onresize = setCardWidth;

    var onClickLink = (e) => {
        e.preventDefault();        
        var target = e.target.getAttribute('href');
        var element = document.querySelector(target);
        var scrollTop = window.pageYOffset || document.documentElement.scrollTop;
        var rect = element.getBoundingClientRect();
        var top = rect.top + scrollTop;

        document.querySelectorAll('.menu .link').forEach( (e) => {
            e.classList.remove('active');
        });
        e.target.classList.add('active');

        var header = document.querySelector('.header');
        if (!header.classList.contains('fixed')) {
            top = top - 210;
        }
        window.scroll({
            top: top,
            left: 0, 
            behavior: 'smooth'
        });
    }

    document.querySelectorAll('.menu .link').forEach( (e) => {
        e.onclick = onClickLink;
    });

    var toggleMenu = () => {
        const menu_cont = document.querySelector('.menu_mobile_cont');
        if ( menu_cont.classList.contains('open') ) {
            menu_cont.style.left = '0';
            setTimeout(() => {
                menu_cont.style.left = '100%';
                setTimeout(() => {
                    menu_cont.className = 'menu_mobile_cont close';
                }, 999);
            }, 1);
        } else {
            menu_cont.className = 'menu_mobile_cont open';
            menu_cont.style.left = '100%';
            setTimeout(() => {
                menu_cont.style.left = '0';
            }, 10);
        }
    };

    document.querySelector('.close_menu_button').onclick = (e) => {
        e.preventDefault();
        toggleMenu();
    };

    document.querySelector('.menu_mobile_button').onclick = (e) => {
        e.preventDefault();
        toggleMenu();
    };

    var onClickLinkMobile = (e) => {
        e.preventDefault();
        toggleMenu();

        var target = e.target.getAttribute('href');
        var element = document.querySelector(target);
        var scrollTop = window.pageYOffset || document.documentElement.scrollTop;
        var rect = element.getBoundingClientRect();
        var top = rect.top + scrollTop;

        document.querySelectorAll('.menu .link').forEach( (e) => {
            e.classList.remove('active');
        });
        e.target.classList.add('active');

        var header = document.querySelector('.header');
        if (!header.classList.contains('fixed')) {
            top = top - 220;
        } else {
            top = top - 110;
        }
        window.scroll({
            top: top,
            left: 0, 
            behavior: 'smooth'
        });
    };

    document.querySelectorAll('.menu_mobile .link').forEach( (e) => {
        e.onclick = onClickLinkMobile;
    });

    var snackbar = document.getElementById("snackbar");
    var showSnackbar = (message) => {
        snackbar.innerHTML = message;
        snackbar.className = "show";
        setTimeout(function(){ snackbar.className = snackbar.className.replace("show", ""); }, 3000);
    };

    var postAjax = (url, data, success) => {
        var params = typeof data == 'string' ? data : Object.keys(data).map(
                function(k){ return encodeURIComponent(k) + '=' + encodeURIComponent(data[k]) }
            ).join('&');
    
        var xhr = window.XMLHttpRequest ? new XMLHttpRequest() : new ActiveXObject("Microsoft.XMLHTTP");
        xhr.open('POST', url);
        xhr.onreadystatechange = function() {
            if (xhr.readyState>3 && xhr.status==200) { success(xhr.responseText); }
        };
        xhr.setRequestHeader('X-Requested-With', 'XMLHttpRequest');
        xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
        xhr.send(params);
        return xhr;
    }

    var formsubmit = (e) => {
        e.preventDefault();

        var name = document.getElementById('form_name').value;
        var email = document.getElementById('form_email').value;
        var phone = document.getElementById('form_phone').value;
        var subject = document.getElementById('form_subject').value;
        var message = document.getElementById('form_message').value;

        if ( name.length < 6) {
            showSnackbar('Nombre no valido, muy corto');
            return;
        }
        if ( email.length < 6) {
            showSnackbar('Email no valido');
            return;
        }
        if ( phone.length < 8) {
            showSnackbar('Teléfono no valido');
            return;
        }
        if ( subject.length < 10) {
            showSnackbar('Asunto muy corto');
            return;
        }
        if ( message.length < 10) {
            showSnackbar('Mensaje muy corto');
            return;
        }

        if ( name.length > 100) {
            showSnackbar('Nombre muy largo');
            return;
        }
        if ( email.length > 100) {
            showSnackbar('Email muy largo');
            return;
        }
        if ( phone.length > 9) {
            showSnackbar('Teléfono muy largo');
            return;
        }
        if ( subject.length > 100) {
            showSnackbar('Asunto muy largo');
            return;
        }
        if ( message.length > 500) {
            showSnackbar('Mensaje muy largo');
            return;
        }

        var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        if (!re.test(String(email).toLowerCase()) ) {
            showSnackbar('Email no valido');
            return;
        }

        if (isNaN(Number(phone))) {
            showSnackbar('Teléfono no valido');
            return;
        }

        var data = {
            name: name,
            email: email,
            phone: phone,
            subject: subject,
            message: message
        };

        fetch('/registro', {
            method: 'POST', // or 'PUT'
            body: JSON.stringify(data), // data can be `string` or {object}!
            headers:{
                'X-CSRF-TOKEN': document.querySelector('input[name=_token]').value,
                'Content-Type': 'application/json'
            }
          }).then( (res) => {
              if (res.status == 200) {
                showSnackbar('Se envio con éxito');
                document.getElementById('form_name').value = '';
                document.getElementById('form_email').value = '';
                document.getElementById('form_phone').value = '';
                document.getElementById('form_subject').value = '';
                document.getElementById('form_message').value = '';
              } else {
                showSnackbar('Ocurrio un error');
              }
            })
          .catch(error => showSnackbar('Ocurrio un error'))
          .then((response) => {
              if (res.status == 200) {
                showSnackbar('Se envio con éxito');
                document.getElementById('form_name').value = '';
                document.getElementById('form_email').value = '';
                document.getElementById('form_phone').value = '';
                document.getElementById('form_subject').value = '';
                document.getElementById('form_message').value = '';
              } else {
                showSnackbar('Ocurrio un error');
              }
            });
    }

    document.querySelector('.submit-button').onclick = formsubmit;

    var dropDownClick = (el) => {
        el.preventDefault();
        document.querySelectorAll('.dropdown').forEach( (e) => {
            e.parentElement.className = 'item close';
        });
        el.target.parentElement.parentElement.className = 'item open';
    };

    document.querySelectorAll('.dropdown').forEach( (e) => {
        e.onclick = dropDownClick;
    });
    
    let currentProgram = 1;
    var nextProgram = () => {
        currentProgram = currentProgram + 1;
        if ( currentProgram === 4) {
            currentProgram = 1;
        }
        gotoProgram(currentProgram);
    };

    var prevProgram = () => {
        currentProgram = currentProgram - 1;
        if ( currentProgram === 0) {
            currentProgram = 3;
        }
        gotoProgram(currentProgram);
    };

    var gotoProgram = (i) => {
        document.getElementById('module1').className = 'moduletitle';
        document.getElementById('module2').className = 'moduletitle';
        document.getElementById('module3').className = 'moduletitle';
        document.getElementById('module' + i).className = 'moduletitle active';
        document.getElementById('module1cont').style.display = 'none';
        document.getElementById('module2cont').style.display = 'none';
        document.getElementById('module3cont').style.display = 'none';
        document.getElementById('module'+i+'cont').style.display = 'block';
        document.getElementById('module'+i+'cont').style.opacity = '0';
        setTimeout(() => {
            document.getElementById('module'+i+'cont').style.opacity = '1';
        }, 1);

    };

    var getProgram = (el) => {
        gotoProgram(el.target.getAttribute('id').split('module')[1]);
    };

    document.getElementById('module1').onclick = getProgram;
    document.getElementById('module2').onclick = getProgram;
    document.getElementById('module3').onclick = getProgram;

    document.querySelectorAll('.schedule .header .prev').forEach((e)=> {
        e.onclick = prevProgram;
    });
    document.querySelectorAll('.schedule .header .next').forEach((e)=> {
        e.onclick = nextProgram;
    });

};

